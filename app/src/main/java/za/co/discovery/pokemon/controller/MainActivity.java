package za.co.discovery.pokemon.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import za.co.discovery.pokemon.R;
import za.co.discovery.pokemon.adapter.PokemonCustomAdapter;
import za.co.discovery.pokemon.helper.ApiClient;
import za.co.discovery.pokemon.helper.MyDividerItemDecoration;
import za.co.discovery.pokemon.helper.PokeApi;
import za.co.discovery.pokemon.helper.Utils;
import za.co.discovery.pokemon.model.Data;
import za.co.discovery.pokemon.model.Pokemon;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Pokemon> pokemonList = new ArrayList<>();
    private static final String TAG = MainActivity.class.getSimpleName();
    private PokeApi pokeApi;
    private CompositeDisposable disposable = new CompositeDisposable();
    private RecyclerView recyclerView;
    private TextView noPokemonView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //UI elements initialization
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        recyclerView = findViewById(R.id.recycler_view);
        noPokemonView = findViewById(R.id.text_empty);

        pokeApi = ApiClient.getClient(getApplicationContext()).create(PokeApi.class);


        //Check for internet connection
        checkInternetConnection();

        //Refresh swipe for random pokemon
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }

    private void checkInternetConnection() {
        if (!Utils.isOnline(MainActivity.this)) {
            CreateAlertDialog("Internet Connection", "No internet connection");
        }else {
            //Making a Api call method
            fetchAllPokemons();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_refresh) {
            // signal SwipeRefreshLayout to start the progress indicator
            swipeRefreshLayout.setRefreshing(true);
            refreshContent();
            // signal SwipeRefreshLayout to stop the progress indicator
            swipeRefreshLayout.setRefreshing(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Fetching all pokemons from api
     */
    private void fetchAllPokemons() {

        // Set up progress before call
        final ProgressDialog progressDoalog = new ProgressDialog(MainActivity.this);
        progressDoalog.setMessage("Initializing data....");
        progressDoalog.setTitle("Pokemon");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // show it
        progressDoalog.show();

        disposable.add(pokeApi.getAllPokemons()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Data>() {
                        @Override
                        public void onSuccess(Data data) {
                            Log.w(TAG, "Poke "+ data.getResults().size());
                            pokemonList = addToList(data);
                            progressDoalog.dismiss();
                            showEmptyPokemons();
                        }

                        @Override
                        public void onError(Throwable t) {
                            progressDoalog.dismiss();
                            Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                );
    }


    //refresh pokemons list
    private void refreshContent(){

        pokemonList.clear();
        fetchAllPokemons();


    }

    private ArrayList<Pokemon> addToList(Data data) {
         ArrayList<Pokemon> pokemonList = new ArrayList<>();
            for (int i = 0; i < data.getResults().size(); i++) {

               Pokemon pokemon = new Pokemon(data.getResults().get(i).getName(),data.getResults().get(i).getUrl());
               pokemonList.add(pokemon);
            }

          return pokemonList;

    }

    private void showEmptyPokemons() {
        if (pokemonList.size() > 0) {
            Log.w(TAG, "Pokemon show ");
            noPokemonView.setVisibility(View.GONE);

            PokemonCustomAdapter mAdapter = new PokemonCustomAdapter(this, pokemonList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();

        } else {
            noPokemonView.setVisibility(View.VISIBLE);
        }

    }


    public void CreateAlertDialog(final String title, final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this, R.style.MyAlertDialogStyle)
                        .setTitle(title)
                        .setMessage(message)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                checkInternetConnection();
                            }
                        })
                        .setCancelable(false)
                        .show();
            }
        });
    }
}

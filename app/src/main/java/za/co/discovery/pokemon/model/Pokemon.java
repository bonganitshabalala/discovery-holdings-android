package za.co.discovery.pokemon.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Pokemon implements Serializable{

    @SerializedName("name")
    private String name;

    @SerializedName("url")
    private String url;

    public Pokemon(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

}

package za.co.discovery.pokemon.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("results")
    private List<Pokemon> results = null;

    public List<Pokemon> getResults() {
        return results;
    }

}

package za.co.discovery.pokemon.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import za.co.discovery.pokemon.R;
import za.co.discovery.pokemon.helper.Constants;
import za.co.discovery.pokemon.model.Pokemon;

public class PokemonCustomAdapter extends RecyclerView.Adapter<PokemonCustomAdapter.MyViewHolder> {

    private Context mContext;
    private List<Pokemon> pokemons;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView mIcon;
        private TextView mText;

        MyViewHolder(View view) {
            super(view);
            mIcon = view.findViewById(R.id.imageView);
            mText = view.findViewById(R.id.textView);
        }
    }


    //Constructor
    public PokemonCustomAdapter(Context context, List<Pokemon> pokemons){
        this.mContext = context;
        this.pokemons = pokemons;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_detail_structure, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Pokemon pokemon = pokemons.get(position);

        holder.mText.setText(pokemon.getName());
        String url = pokemon.getUrl();
        String[] url_array = url.split("/");
        // Displaying image
            String image = Constants.SPRITES_URL + url_array[6] + ".png";
            if (image.equals("null")) {

                Picasso.with(mContext).load(R.drawable.blank).into(holder.mIcon);
            } else {
                Picasso.with(mContext).load(image).into(holder.mIcon);
            }




    }

    @Override
    public int getItemCount() {
        return pokemons.size();
    }


}
package za.co.discovery.pokemon.helper;


import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import za.co.discovery.pokemon.model.Data;


public interface PokeApi {

    // Get all pokemons
    @GET("pokemon/?limit=127&offset=1")
    Single<Data> getAllPokemons();

    // Get a single pokemonImage
    @GET("https://github.com/PokeAPI/sprites/blob/master/sprites/pokemon/{id}")
    String getPokemonImage(@Path("id") String index);


}

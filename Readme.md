# Discovery Android technical assessment

## Getting started

Please create a feature branch for yourself and complete the exercise. Once you are done, create a pull request back into
the master branch.

## What to do

Use the Pokémon API that can be found at https://www.pokeapi.co/ to display a list of Pokémon in an app. Sprites for these
Pokémon are available on Github at https://github.com/PokeAPI/sprites (the API and the sprites are maintained by the same people).
Please make sure that your app works on API 16+ and that it does not crash on low-memory devices.

## A couple of notes:

- There are no wrong answers. This is simply to give us an idea of your skill level
- You are welcome to use Kotlin
- We've added some dependencies to the build.gradle file. You can remove those and use libraries of your choice if you are not comfortable with the ones we selected
- If you do anything out of the ordinary, add a comment explaining why you did it that way